LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := demo

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS := -lemb -lpthread -lrt -ldl

LOCAL_LIB_PATHS :=

LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/opensource/libemb

LOCAL_SRC_FILES := \
	demo.cpp
	
LOCAL_PRE_BUILD := 
LOCAL_POST_BUILD :=
include $(BUILD_EXECUTABLE)

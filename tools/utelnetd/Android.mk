LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= 
LOCAL_SRC_FILES:= utelnetd.c
LOCAL_MODULE := utelnetd
LOCAL_SHARED_LIBRARIES:= libcutils libutils
LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS += -Wformat-security -pipe -DSHELLPATH=\"/system/bin/sh\"

include $(BUILD_EXECUTABLE)
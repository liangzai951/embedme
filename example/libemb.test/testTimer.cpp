#include "BaseType.h"
#include "Tracer.h"
#include "ArgUtil.h"
#include "DateTime.h"
#include "Timer.h"
using namespace libemb;

class RTimerTest:public TimerListener{
private:
	Thread m_thread;
	std::unique_ptr<RTimer> m_timer;
	Time m_lastTime;
public:
    RTimerTest()
    {
        /* 创建定时器 */
        m_timer = std::make_unique<RTimer>(m_thread,*this,0x01);
    }
    ~RTimerTest()
    {
    }
    void onTimer(int timerId)
    {
    	Time currTime = Time::currentTimeMonotonic();
		Time diffTime = currTime-m_lastTime;
		int interval = diffTime.toMicroSeconds();
		TRACE_DBG_CLASS("Timer:%d at %s, interval:%d\n",timerId,CSTR(currTime.toString()),interval);
		m_lastTime = currTime;
    }
    void startTest()
    {
        /* 启用定时器 */
        m_timer->start(2000,true);
		m_lastTime = Time::currentTimeMonotonic();
    }
    void stopTest()
    {
        /* 停止定时器 */
        m_timer->stop();
        TRACE_ERR_CLASS("m_timer stopped.\n");
    }
};
void testTimer(void)
{
	TRACE_INFO("timer test start >>>>>>\n");
    std::unique_ptr<RTimerTest> test = std::make_unique<RTimerTest>();
    while(1)
    {
        TRACE_CYAN("====Test Menu====\n");
        TRACE_CYAN("01. start RTimer.\n");
        TRACE_CYAN("02. stop RTimer.\n");
        InputGet mi;
		mi.waitInput();
        if (mi.match("q") || mi.match("quit")) 
        {
            break;
        }
        else if(mi.match("01"))
        {
            test->startTest();
        }
        else if(mi.match("02"))
        {
            test->stopTest();
        }
    }
	test->stopTest();
	TRACE_INFO("timer test finished <<<<<<\n");
}


/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __CAN_PORT_H__
#define __CAN_PORT_H__

#include "BaseType.h"
#include "libsocketcan.h"
namespace libembx{

enum CANSTATE_E
{
	CANSTATE_ERROR_ACTIVE=CAN_STATE_ERROR_ACTIVE,	/* 0:RX/TX error count < 96 */
	CANSTATE_ERROR_WARNING=CAN_STATE_ERROR_WARNING,	/* 1:RX/TX error count < 128 */
	CANSTATE_ERROR_PASSIVE=CAN_STATE_ERROR_PASSIVE,	/* 2:RX/TX error count < 256 */
	CANSTATE_BUS_OFF=CAN_STATE_BUS_OFF,				/* 3:RX/TX error count >= 256 */
	CANSTATE_STOPPED=CAN_STATE_STOPPED,				/* 4:Device is stopped */
	CANSTATE_SLEEPING=CAN_STATE_SLEEPING,			/* 5:Device is sleeping */
};

class CANPort{
public:
	CANPort();
	~CANPort();
	bool open(const char* name, int bitrate);
	void close();
	void restart();
	int getState();
private:
	std::string m_name;
};
}
#endif

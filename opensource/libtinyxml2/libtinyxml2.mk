LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_LIB_TYPE:=static

LOCAL_BUILD_DIR:=tinyxml2
LOCAL_BUILD_MAKEFILE:=Makefile.mbuild

include $(BUILD_MBMAKE)
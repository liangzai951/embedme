/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __DATETIME_H__
#define __DATETIME_H__

#include "BaseType.h"
#include <iostream>

namespace libemb{

class Time{
public:	
	Time();
	Time(int sec,int us);
	virtual ~Time();
	int secondPart();
	int microSecondPart();
	std::string toString();
	long long toMicroSeconds();
	static Time currentTimeFromEpoch(); /* 相对1970-01-01 00:00:00 UTC的时间 */
	static Time currentTimeMonotonic(); /* 系统绝对起始时间 */
	static int usIntervalMonotonic(Time& monoStartTime);
	Time operator +(const Time &t);
	Time operator -(const Time &t);
	
private:
	int m_seconds{0};
	int m_microSeconds{0};
};

/**
 *  \class  DateTime
 *  \brief  时间结构体      
 */
class DateTime{
public:
    DateTime();
    DateTime(unsigned int year,unsigned int month,unsigned int date,unsigned int hour,unsigned int minute,unsigned int second);
    DateTime(const DateTime& copy);
    ~DateTime();
    unsigned int year();    /* 年(dddd) */   
    unsigned int month();   /* 月(1~12) */
    unsigned int date();    /* 日(1~31) */
    unsigned int hour();    /* 时(0~23) */
    unsigned int minute();  /* 分(0~59) */
    unsigned int second();  /* 秒(0~59) */
	unsigned int weekday();/* 星期(0~6:星期日~星期六) */
    DateTime& setYear(unsigned int year);
    DateTime& setMonth(unsigned int month);
    DateTime& setDate(unsigned int date);
    DateTime& setHour(unsigned int hour);
    DateTime& setMinute(unsigned int minute);
    DateTime& setSecond(unsigned int second);
    std::string toString(); /* 转换为字符串格式:"yyyy-mm-dd hh:MM:ss" */
	bool isLeapYear();
    static DateTime currentDateTime();
    static int setCurrentDateTime(DateTime& dateTime);
    static DateTime getRTCDateTime();
    static int setRTCDateTime(DateTime& dateTime);
private:
    unsigned int m_second{0};   
    unsigned int m_minute{0};   
    unsigned int m_hour{0};     
    unsigned int m_date{1};     
    unsigned int m_month{1};    
    unsigned int m_year{2000};     
};
}
#endif

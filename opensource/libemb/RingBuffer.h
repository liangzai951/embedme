/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __RINGBUFFER_H__
#define __RINGBUFFER_H__

#include "BaseType.h"
#include "ThreadUtil.h"

namespace libemb{
class RingBuffer{
public:
	RingBuffer();
	~RingBuffer();
	bool init(int capacity);
	void reset();
	int capacity();		/* 获取总容量 */
	int space();		/* 获取剩余空间,=0表示缓存满 */
	int size();			/* 获取数据长度,=0表示缓存空 */
	int read(char *data, int bytes);
	int write(char *data, int bytes);
private:
	int m_startPos{0};
	int m_endPos{0};
	int m_occupant{0};
	int m_capacity{0};
	std::unique_ptr<char[]> m_bufPtr{nullptr};
	libemb::Mutex m_mutex;
};
}
#endif

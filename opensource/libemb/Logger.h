/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "BaseType.h"
#include "Singleton.h"
#include "FileUtil.h"
#include <map>
namespace libemb{
class Logger:public libemb::Singleton<Logger>{
DECL_SINGLETON(Logger);
public:
	virtual ~Logger();
	void setRoot(std::string logDir);
	bool openLog(std::string logName);
	void closeLog(std::string logName);
	bool log(std::string logName, const char* format,...);
private:
	std::shared_ptr<File> getLogFile(std::string logName);
private:
	std::map<std::string, std::shared_ptr<File>> m_logMap;
	std::string m_logDir{""};
	std::unique_ptr<char[]> m_logBufPtr;
};

}

#endif


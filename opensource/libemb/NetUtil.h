/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __NET_UTIL_H__
#define __NET_UTIL_H__

#ifdef OS_CYGWIN
#else
#include "BaseType.h"

namespace libemb{
enum class NetHwState
{
	Down,		/**< 网口已被ifconfig down */
	Up,			/**< 网口已被ifconfig up */
	Error
};

enum class NetLinkState
{
	Disconnected,	/**< 网口未插入网线,link down */
	Connected, 	/**< 网口已插入网线,link up */
	Error
};

/**
 *  \file   NetUtil.h   
 *  \class  NetUtil
 *  \brief  网络类,提供对网络设别的常用操作.
 */
class NetUtil{
public:
    NetUtil();
    ~NetUtil();
    static bool isValidIP(const std::string ip);
    static bool checkSubnetIP(const std::string gatewayIP,const std::string subnetMask,std::string& subnetIP);
    static std::string getSubnetIP(const std::string gatewayIP,const std::string subnetMask,int subIndex);
    static std::string getInetAddr(const std::string intf);
    static std::string getMaskAddr(const std::string intf);
    static std::string getMacAddr(const std::string intf);
	static NetHwState getHwState(const std::string intf);
    static NetLinkState getLinkState(const std::string intf);
    static bool setInetAddr(const std::string intf,const std::string ipaddr);
    static bool setMaskAddr(const std::string intf,const std::string netmask);
    static bool setMacAddr(const std::string intf,const std::string mac);
	static bool setHwState(const std::string intf, NetHwState state);
};
}
#endif
#endif

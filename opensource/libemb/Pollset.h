/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014-2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __POLL_SET_H__
#define __POLL_SET_H__

#include "BaseType.h"
#include "DateTime.h"
#include <sys/epoll.h>

namespace libemb{

class Pollset{
public:
	static const int EventRead = 0; 
	static const int EventWrite = 1; 
public:
	Pollset();
	virtual ~Pollset();
	bool open(int maxEvents,bool et=false);
	void close();
	bool addEvent(int fd, int event);
	bool removeEvent(int fd, int event);
	int waitEvent(IntArray& fdArray, IntArray& eventArray, int msTimeout);
private:
	int m_epfd{-1};
	int m_size{0};
	bool m_edgeTrig{false};
	struct epoll_event* m_events{NULL};
};

}
#endif

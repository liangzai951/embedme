LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libtinyxml
LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_INC_PATHS := \
    $(LOCAL_PATH)

LOCAL_SRC_FILES := \
    tinyxml.cpp \
    tinyxmlparser.cpp \
    tinyxmlerror.cpp \
    tinystr.cpp

LOCAL_LIB_TYPE:=static

include $(BUILD_LIBRARY)
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_LIB_TYPE:=static

LOCAL_SUB_DIR:=libyuv-1514
#SUB_MAKEFILE_PATH:=Makefile.mbuild		# gcc不支持ARM_NEON
SUB_MAKEFILE_PATH:=Makefile-neon.mbuild	# gcc支持ARM_NEON

#SUB_CONFIGURE_SCRIPT:=cp -f include/libyuv/row-4.8.lower.h include/libyuv/row.h	# gcc版本小于4.8
SUB_CONFIGURE_SCRIPT:=cp -f include/libyuv/row-4.8.greater.h include/libyuv/row.h	# gcc版本大于4.8
include $(BUILD_SUBMAKE)